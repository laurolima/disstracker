from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.db import models


class Song(models.Model):
    """
    A song.
    """
    name = models.CharField(max_length=256)
    artist = models.ManyToManyField('Artist', blank=True)
    lyrics = models.TextField(blank=True)
    launch_date = models.DateField(null=True, blank=True)
    is_diss_track = models.BooleanField()

    def __str__(self):
        return self.name

class Album(models.Model):
    """
    A music album.
    """
    name = models.CharField(max_length=256)
    tracks = models.ManyToManyField('Song', blank=True)
    launch_date = models.DateField(null=True, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Artist(models.Model):
    """
    An artist.
    """
    name = models.CharField(max_length=256)
    aliases = ArrayField(
            base_field=models.CharField(max_length=256, default=""),
            blank=True)
    picture = models.ImageField(null=True, blank=True)
    groups = models.ManyToManyField('Group', blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Group(models.Model):
    """
    A group of artists.
    """
    name = models.CharField(max_length=256)
    aliases = ArrayField(
            base_field=models.CharField(max_length=256, default=""),
            blank=True)
    picture = models.ImageField(null=True, blank=True)
    launch_date = models.DateField(null=True, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class LyricsReference(models.Model):
    """
    Represents a lyrics reference to another song, artist, group or album.
    Regular users will be able to add references to lyrics, after moderation.
    """
    note = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
            related_name='annotations', on_delete=models.CASCADE)
    song = models.ForeignKey('Song', on_delete=models.CASCADE, related_name='annotations')
    album_reference = models.ManyToManyField('Album', blank=True)
    artist_reference = models.ManyToManyField('Artist', blank=True)
    song_reference = models.ManyToManyField('Song', blank=True)
    group_reference = models.ManyToManyField('Group', blank=True)

class User(AbstractUser):
    picture = models.ImageField(null=True, blank=True)
