from django.contrib import admin
from app import models as appmodels

# Register your models here.
admin.site.register(appmodels.Song)
admin.site.register(appmodels.Album)
admin.site.register(appmodels.Artist)
admin.site.register(appmodels.Group)
admin.site.register(appmodels.LyricsReference)
admin.site.register(appmodels.User)
