FROM python:3.9.6-slim

WORKDIR /app

COPY . .
RUN pip install -r requirements.txt

CMD ["gunicorn", "myproject.asgi:application", "--host", "0.0.0.0", "-k", "uvicorn.workers.UvicornWorker"]
